exports.webSocketController = function (socket) {
    //var plot = require('./../model/plot');
    return {
        sendAll: function (event, data) {
            return new Promise(function (resolve, reject) {
                socket.emit(event, data)
                resolve();
            });
        },
        sendSpecific: function (id, event, data) {
            return new Promise(function (resolve, reject) {
                socket.to(id).emit(event, data)
                resolve();
            });
        },
    }
}