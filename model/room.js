mongoose = require('mongoose');
var room = mongoose.Schema({
    id: {
        type: String,
        unique: true,
    },
    caller: {
        id: {
            type: String,
            unique: true,
        },
        sdp: {},
        iceCandidates: [{}]
    },
    calleeids: [{
        id: {
            type: String,
            unique: true,
        },
        sdp: {},
        iceCandidates: [{}]
    }],
    datecreated: {
        type: Date,
        default: Date.now
    }

})

module.exports = mongoose.model('room', room);