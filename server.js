var express = require('express');
var path = require("path");
var https = require('https');
var bodyParser = require('body-parser');
var config = require('./main/config');
var fs = require('fs');
var port = process.env.PORT || config.port;
var app = express();
var options = {
    key: fs.readFileSync('/home/ubuntu/.ssh/privkey.pem'),
    cert: fs.readFileSync('/home/ubuntu/.ssh/cert.pem'),
};
var server = https.createServer(options, app);
var io = require('socket.io')(server);
var mongoose = require('mongoose');
mongoose.connect('mongodb://' + config.dbuser + ':' + config.dbpassword + '@ds259258.mlab.com:59258/syllable');

var ioCtrl = require('./controller/IOController');

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, \Authorization');
    next();
});
var api = express.Router();
app.use('/', api);
api.get('/appstart', function (req, res) {
    res.sendFile(path.join(__dirname + '/view/index.html'));
});
api.get('/createroom', function (req, res) {
    roomCtrl = require("./controller/roomController").roomController();
    var roomid = req.query.socketid + "-room";
    var callerid = req.query.socketid;
    roomCtrl.createRoom(roomid, callerid).then(function (result) {
        res.status(200).json({
            roomid: roomid
        })
    }, function (err) {
        res.status(500).send('error:Room not created');
        console.log("In create room function :");
        console.log(err);
    }).catch(function (e) {}

    );
});
io.on('connection', function (socket) {
    ioCtrl.IOController(socket, io);
});
io.on('disconnect', function (socket) {
    console.log("disconnected : socket server");
});
io.on('error', function () {
    console.log("can't connect : socket server");
});
server.listen(port);
console.log(port);