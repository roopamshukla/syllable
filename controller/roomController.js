exports.roomController = function () {
    var room = require('./../model/room');
    return {
        createRoom: function (roomid, callerid) {
            return new Promise(function (resolve, reject) {
                newRoom = new room();
                newRoom.id = roomid;
                newRoom.caller.id = callerid;
                newRoom.save(function (err, res) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(res);
                    }
                });
            });
        },
        deleteRoom: function (roomid) {
            return new Promise(function (resolve, reject) {
                room.remove({
                    id: roomid
                }, function (err, res) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(res);
                    }
                })
            });
        },
        addCallee: function (calleeid, roomid) {
            var callee = {
                id: calleeid
            }
            try {
                return new Promise(function (resolve, reject) {
                    room.findOneAndUpdate({
                        "id": roomid,
                    }, {
                        $push: {
                            "calleeids": callee
                        }
                    }, function (err, res) {
                        if (err) {
                            reject(err);
                        } else if (res) {
                            resolve(res);
                        } else {
                            reject(res);
                        }
                    });

                });
            } catch (e) {
                console.log("error:addCallee" + e);
            }
        },
        setCallerSDP: function (roomid, sdp) {
            try {
                return new Promise(function (resolve, reject) {
                    room.findOneAndUpdate({
                        "id": roomid,
                    }, {
                        $set: {
                            "caller.sdp": sdp
                        }
                    }, function (err, res) {
                        if (err) {
                            reject(err);
                        } else if (res) {
                            resolve(res);
                        } else {
                            reject(res);
                        }
                    });

                });
            } catch (e) {
                console.log("error:setCallerSDP" + e);
            }
        },
        setCallerICE: function (roomid, ICE) {
            try {
                return new Promise(function (resolve, reject) {
                    if (ICE) {
                        room.findOneAndUpdate({
                            "id": roomid,
                        }, {
                            $push: {
                                "caller.iceCandidates": ICE
                            }
                        }, function (err, res) {
                            if (err) {
                                reject(err);
                            } else if (res) {
                                resolve(res);
                            } else {
                                reject(res);
                            }
                        });
                    }
                });
            } catch (e) {
                console.log("error:setCallerICE" + e);
            }
        },
        getCallerSDP: function (roomid) {
            try {
                return new Promise(function (resolve, reject) {

                    room.findOne({
                        "id": roomid,
                    }, function (err, res) {
                        if (err) {
                            reject(err);
                        } else if (res.caller.sdp) {
                            resolve(res.caller.sdp);
                        } else {
                            reject(res);
                        }
                    });

                });
            } catch (e) {
                console.log("error:getCallerSDP" + e);
            }
        },
        getCallerICE: function (roomid) {
            try {
                return new Promise(function (resolve, reject) {
                    room.findOne({
                        "id": roomid,
                    }, function (err, res) {
                        if (err) {
                            reject(err);
                        } else if (res.caller.iceCandidates) {
                            resolve(res.caller.iceCandidates);
                        } else {
                            reject(res);
                        }
                    });

                });
            } catch (e) {
                console.log("error:getCallerICE" + e);
            }
        },
        setCalleeSDP: function (calleeid, roomid, sdp) {
            try {
                return new Promise(function (resolve, reject) {
                    room.findOneAndUpdate({
                        "id": roomid,
                        "calleeids.id": calleeid
                    }, {
                        $set: {
                            "calleeids.$.sdp": sdp
                        }
                    }, function (err, res) {
                        if (err) {
                            reject(err);
                        } else if (res) {
                            resolve(res);
                        } else {
                            reject(res);
                        }
                    });

                });
            } catch (e) {
                console.log("error:setCalleeSDP" + e);
            }
        },
        setCalleeICE: function (calleeid, roomid, ICE) {
            try {
                return new Promise(function (resolve, reject) {
                    if (ICE) {
                        room.findOneAndUpdate({
                            "id": roomid,
                            "calleeids.id": calleeid
                        }, {
                            $push: {
                                "calleeids.$.iceCandidates": ICE
                            }
                        }, function (err, res) {
                            if (err) {
                                reject(err);
                            } else if (res) {
                                resolve(res);
                            } else {
                                reject(res);
                            }
                        });
                    }
                });
            } catch (e) {
                console.log("error:setCalleeICE" + e);
            }
        },
    }
}