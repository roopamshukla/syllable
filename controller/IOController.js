exports.IOController = function (socket, io) {
    var roomCtrl = require("./roomController").roomController();
    var wsCtrl = require("./webSocketController").webSocketController(socket);
    wsCtrl.sendAll("handshake", "you have been pinged");
    socket.on("joinRoom", function (data) {
        socket.join(data.roomid, function () {
            if (!data.isCaller) {
                roomCtrl.addCallee(socket.id, data.roomid).then(function () {
                    roomCtrl.getCallerSDP(data.roomid).then(function (callerSDP) {
                        wsCtrl.sendAll("getSDP", callerSDP).then(function () {
                            roomCtrl.getCallerICE(data.roomid).then(function (callerICE) {
                                wsCtrl.sendAll("getICE", callerICE).then(function () {

                                });
                            }).catch(function (e) {

                            });
                        });
                    }).catch(function (e) {

                    });
                }).catch(function (e) {

                });
            }
        });
    });
    socket.on("setSDP", function (data) {
        roomCtrl.setCallerSDP(data.roomid, data.sdp).catch(function (e) {

        });
    });
    socket.on("setICE", function (data) {
        roomCtrl.setCallerICE(data.roomid, data.candidate).catch(function (e) {

        });
    });
    socket.on("setCalleeSDP", function (data) {
        roomCtrl.setCalleeSDP(socket.id, data.roomid, data.sdp).then(function () {
            wsCtrl.sendSpecific(data.roomid.replace('-room', ''), 'setBackSDP', data.sdp);
        }).catch(function (e) {});
    });
    socket.on("setCalleeICE", function (data) {
        roomCtrl.setCalleeICE(socket.id, data.roomid, data.candidate).then(function () {
            wsCtrl.sendSpecific(data.roomid.replace('-room', ''), 'setBackICE', data.candidate);
        }).catch(function (e) {});
    });
}